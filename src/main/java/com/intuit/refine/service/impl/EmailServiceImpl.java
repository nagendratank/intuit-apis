package com.intuit.refine.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.intuit.refine.jpa.entities.RuleEntity;
import com.intuit.refine.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService {

	@Override
	public void sendmail(RuleEntity ruleEntity) throws AddressException, MessagingException, IOException {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("nagendertank@gmail.com", "qmuuxhxgayhyywxq");
			}
		});
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("nagendra@intuit-test.com", false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(ruleEntity.getEmail()));
		msg.setSubject(ruleEntity.getRuleName() + " rule is applied successfully");
		msg.setContent(
				"Hi, Please login to system to see the result. You are recieving this email because you are part of "
						+ ruleEntity.getRuleName() + " rule. The action corresponding to rule " + ruleEntity.getAction()
						+ " is applied.",
				"text/html");
		msg.setSentDate(new Date());
		Transport.send(msg);
	}

}
