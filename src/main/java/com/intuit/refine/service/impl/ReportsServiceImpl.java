package com.intuit.refine.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.WorkdayReport;
import com.intuit.refine.jpa.repository.AlightReportDAO;
import com.intuit.refine.jpa.repository.WorkdayReporDAO;
import com.intuit.refine.service.ReportsService;
import com.intuit.refine.utils.IntuitExceptions;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Service
public class ReportsServiceImpl implements ReportsService {
	private static Logger LOG = LogManager.getLogger(ReportsServiceImpl.class);

	@Autowired
	private WorkdayReporDAO workdayReporDAO;

	@Autowired
	private AlightReportDAO alightReportDAO;

	@Override
	public void saveWorkDayReport(MultipartFile file) {
		LOG.info("Inside save report file for Workday");
		try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
			CsvToBean<WorkdayReport> csvToBean = new CsvToBeanBuilder<WorkdayReport>(reader)
					.withType(WorkdayReport.class).withIgnoreLeadingWhiteSpace(true).build();

			List<WorkdayReport> users = csvToBean.parse();
			this.workdayReporDAO.saveAll(users);
			LOG.info("Workday report saved successfully");

		} catch (Exception ex) {
			LOG.error("Failing while saving the records for Workday " + ex);
			throw new IntuitExceptions(ex);
		}
	}

	@Override
	public void saveAlightReport(MultipartFile file) {
		LOG.info("Inside save report file for Alight");
		try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
			CsvToBean<AlightReport> csvToBean = new CsvToBeanBuilder<AlightReport>(reader).withType(AlightReport.class)
					.withIgnoreLeadingWhiteSpace(true).build();

			List<AlightReport> users = csvToBean.parse();
			this.alightReportDAO.saveAll(users);
			LOG.info("Alight report saved successfully");

		} catch (Exception ex) {
			LOG.error("Failing while saving the records for Alight " + ex);
			throw new IntuitExceptions(ex);
		}
	}

	@Override
	public List<WorkdayReport> getAllWorkDayReport() {
		return this.workdayReporDAO.findAll(Sort.by("employeeId"));
	}

	@Override
	public List<AlightReport> getAllAlightReport() {
		return this.alightReportDAO.findAll(Sort.by("employeeId"));
	}

}
