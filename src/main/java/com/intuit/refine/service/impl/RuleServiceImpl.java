package com.intuit.refine.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.RuleEntity;
import com.intuit.refine.jpa.entities.WorkdayReport;
import com.intuit.refine.jpa.repository.AlightReportDAO;
import com.intuit.refine.jpa.repository.RulesDAO;
import com.intuit.refine.jpa.repository.WorkdayReporDAO;
import com.intuit.refine.service.EmailService;
import com.intuit.refine.service.RuleService;

@Service
public class RuleServiceImpl implements RuleService {

	@Autowired
	private RulesDAO rulesDAO;

	@Autowired
	private WorkdayReporDAO workdayReporDAO;

	@Autowired
	private AlightReportDAO alightReportDAO;

	@Autowired
	private EmailService emailService;

	@Override
	public void saveRule(RuleEntity entity) {
		this.rulesDAO.save(entity);
	}

	@Override
	public void deleteRule(int id) {
		this.rulesDAO.deleteById(id);
	}

	@Override
	public List<RuleEntity> getAllRule() {
		return this.rulesDAO.findAll(Sort.by("priority"));
	}

	@Override
	public Optional<RuleEntity> getAllRule(int ruleId) {
		return this.rulesDAO.findById(ruleId);
	}

	@Override
	public String runAllRule() {
		// Clear All Rules
		this.clearAllRules();
		// Apply Rules on Dataset
		this.applyRules();

		return "Rule Applied Successfully";
	}

	private void applyRules() {
		List<RuleEntity> ruleEntities = this.getAllRule();
		ruleEntities.forEach(ruleEntity -> {
			String condition = ruleEntity.getCondition().replace("\"", "").replace("EMPTY", "NULL");
			List<WorkdayReport> employees = this.workdayReporDAO.findEmployeeByCondition(condition);
			List<AlightReport> alightEmployee = new ArrayList<AlightReport>();
			if (employees.size() > 0) {
				employees.forEach(emp -> {
					emp.setRule(ruleEntity);
					AlightReport alEmp = this.alightReportDAO.findById(emp.getEmployeeId()).orElse(null);
					if (alEmp != null) {
						alEmp.setRule(ruleEntity);
						alEmp.setBenefitCode(ruleEntity.getAction());
						alightEmployee.add(alEmp);
					}
				});
			}

			this.workdayReporDAO.saveAll(employees);
			this.alightReportDAO.saveAll(alightEmployee);

			try {
				if (StringUtils.isNotEmpty(ruleEntity.getEmail())) {
					emailService.sendmail(ruleEntity);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private void clearAllRules() {
		List<WorkdayReport> allWorkDayEmployees = this.workdayReporDAO.findAll();
		allWorkDayEmployees.forEach(emp -> {
			emp.setRule(null);
		});
		this.workdayReporDAO.saveAll(allWorkDayEmployees);

		List<AlightReport> alightEmployees = this.alightReportDAO.findAll();
		alightEmployees.forEach(emp -> {
			emp.setRule(null);
		});
		this.alightReportDAO.saveAll(alightEmployees);
	}

}
