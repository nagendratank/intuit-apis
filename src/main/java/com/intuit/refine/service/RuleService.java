package com.intuit.refine.service;

import java.util.List;
import java.util.Optional;

import com.intuit.refine.jpa.entities.RuleEntity;

public interface RuleService {
	void saveRule(RuleEntity entity);

	Optional<RuleEntity> getAllRule(int ruleId);

	List<RuleEntity> getAllRule();

	String runAllRule();

	void deleteRule(int id);
}
