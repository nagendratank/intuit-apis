package com.intuit.refine.service;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.intuit.refine.jpa.entities.RuleEntity;

public interface EmailService {
	void sendmail(RuleEntity ruleEntity) throws AddressException, MessagingException, IOException;
}
