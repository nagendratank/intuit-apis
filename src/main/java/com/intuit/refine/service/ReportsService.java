package com.intuit.refine.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.WorkdayReport;

public interface ReportsService {

	public void saveWorkDayReport(MultipartFile file);

	// void saveAlightReport(MultipartFile file);

	List<WorkdayReport> getAllWorkDayReport();

	List<AlightReport> getAllAlightReport();

	void saveAlightReport(MultipartFile file);
}
