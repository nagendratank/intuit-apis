package com.intuit.refine;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = false, prePostEnabled = false)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().configurationSource(request -> {
			CorsConfiguration configuration = new CorsConfiguration();
			configuration.addAllowedMethod(HttpMethod.PUT);
			configuration.addAllowedMethod(HttpMethod.POST);
			configuration.addAllowedMethod(HttpMethod.GET);
			configuration.addAllowedMethod(HttpMethod.DELETE);
			configuration = configuration.applyPermitDefaultValues();
			return configuration;

		}).and().csrf().disable();
	}
}
