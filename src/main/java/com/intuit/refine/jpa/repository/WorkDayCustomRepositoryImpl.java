package com.intuit.refine.jpa.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.intuit.refine.jpa.entities.WorkdayReport;

public class WorkDayCustomRepositoryImpl implements WorkDayCustomRepository {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<WorkdayReport> findEmployeeByCondition(String condition) {
		Query queryEmployeeByFirstName = entityManager.createNativeQuery(
				"SELECT * FROM workday_report WHERE rule_id IS NULL AND " + condition, WorkdayReport.class);

		return queryEmployeeByFirstName.getResultList();
	}

}
