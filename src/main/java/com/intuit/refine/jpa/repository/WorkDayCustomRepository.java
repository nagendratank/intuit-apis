package com.intuit.refine.jpa.repository;

import java.util.List;

import com.intuit.refine.jpa.entities.WorkdayReport;

public interface WorkDayCustomRepository {

	List<WorkdayReport> findEmployeeByCondition(String condition);

}
