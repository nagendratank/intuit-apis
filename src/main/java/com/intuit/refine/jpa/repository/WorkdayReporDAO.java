package com.intuit.refine.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.intuit.refine.jpa.entities.WorkdayReport;

public interface WorkdayReporDAO extends JpaRepository<WorkdayReport, Long>, WorkDayCustomRepository {

	List<WorkdayReport> findAllByOrderByEmployeeIdDesc();
}
