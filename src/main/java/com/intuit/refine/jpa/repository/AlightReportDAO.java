package com.intuit.refine.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.intuit.refine.jpa.entities.AlightReport;

public interface AlightReportDAO extends JpaRepository<AlightReport, Long> {

	List<AlightReport> findAllByOrderByEmployeeIdDesc();

}
