package com.intuit.refine.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.intuit.refine.jpa.entities.RuleEntity;

public interface RulesDAO extends JpaRepository<RuleEntity, Integer> {

}
