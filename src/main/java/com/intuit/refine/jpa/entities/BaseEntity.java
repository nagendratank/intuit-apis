package com.intuit.refine.jpa.entities;

import java.io.Serializable;

public interface BaseEntity extends Serializable {

    public Serializable getId();
}