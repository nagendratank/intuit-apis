package com.intuit.refine.jpa.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

@Entity
@Table(name = "Workday_Report")
public class WorkdayReport {

	@Id
	@Column(name = "employee_id", unique = true, nullable = false)
	@CsvBindByName(column = "Employee_Id", required = true)
	private long employeeId;

	@Column(name = "first_name", nullable = false)
	@CsvBindByName(column = "First_Name")
	private String firstName;

	@Column(name = "last_name", nullable = false)
	@CsvBindByName(column = "Last_Name")
	private String lastName;

	@Column(name = "employment_type", nullable = false)
	@CsvBindByName(column = "Employment_Type")
	private String employmentType;

	@Column(name = "hire_date", nullable = true)
	@CsvBindByName(column = "Hire_Date")
	@CsvDate(value = "MM/dd/yy")
	private Date hireDate;

	@Column(name = "termination_date", nullable = true)
	@CsvBindByName(column = "Termination_Date")
	@CsvDate(value = "MM/dd/yy")
	private Date terminationDate;

	@Column(name = "termination_status", nullable = true)
	@CsvBindByName(column = "Termination_Status")
	private boolean terminationStatus;

	@Column(name = "termination_reason", nullable = true)
	@CsvBindByName(column = "Termination_Reason")
	private String terminationReason;

	@Column(name = "leave_status", nullable = true)
	@CsvBindByName(column = "Leave_Status")
	private boolean leaveStatus;

	@Column(name = "leave_type", nullable = true)
	@CsvBindByName(column = "Leave_Type")
	private String leaveType;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "rule_id", nullable = true, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key (rule_id) REFERENCES rules ON DELETE SET NULL"))
	private RuleEntity rule = null;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public boolean isTerminationStatus() {
		return terminationStatus;
	}

	public void setTerminationStatus(boolean terminationStatus) {
		this.terminationStatus = terminationStatus;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public boolean isLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(boolean leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public RuleEntity getRule() {
		return rule;
	}

	public void setRule(RuleEntity rule) {
		this.rule = rule;
	}

}
