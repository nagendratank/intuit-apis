package com.intuit.refine.jpa.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

@Entity
@Table(name = "Alight_Report")
public class AlightReport {

	@Id
	@Column(name = "EE_ID", unique = true, nullable = false)
	@CsvBindByName(column = "EE_ID", required = true)
	private Long employeeId;

	@Column(name = "FIRST_NAME", nullable = false)
	@CsvBindByName(column = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME", nullable = false)
	@CsvBindByName(column = "LAST_NAME")
	private String lastName;

	@Column(name = "HIRE_DT", nullable = true)
	@CsvBindByName(column = "HIRE_DT")
	@CsvDate(value = "MM/dd/yy")
	private Date hireDate;

	@Column(name = "TERM_DT", nullable = true)
	@CsvBindByName(column = "TERM_DT")
	@CsvDate(value = "MM/dd/yy")
	private Date terminationDate;

	@Column(name = "BENEFITS_STATUS_CODE", nullable = true)
	private String benefitCode;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "rule_id", nullable = true, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key (rule_id) REFERENCES rules ON DELETE SET NULL"))
	private RuleEntity rule;

	public String getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}

	public RuleEntity getRule() {
		return rule;
	}

	public void setRule(RuleEntity rule) {
		this.rule = rule;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

}
