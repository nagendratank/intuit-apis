package com.intuit.refine.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "rules")
public class RuleEntity {

	@Id
	@GeneratedValue
	@Column(name = "rule_id")
	private int ruleId;

	@Column(name = "ruleName", nullable = false)
	private String ruleName;

	@Column(name = "condition", nullable = false)
	private String condition;
	@Column(name = "action", nullable = true)
	private String action;

	@Column(name = "secondaryAction", nullable = true)
	private String secondaryAction;

	@GeneratedValue
	@Column(name = "priority", nullable = false)
	private Integer priority;

	@Column(name = "description", nullable = true)
	private String description;

	@Lob
	@Column(name = "tree", nullable = false)
	private String tree;

	@Column(name = "color", nullable = false)
	private String color;

	@Column(name = "email", nullable = true)
	private String email;

	public int getRuleId() {
		return ruleId;
	}

	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}

	public String getCondition() {
		return condition;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSecondaryAction() {
		return secondaryAction;
	}

	public void setSecondaryAction(String secondaryAction) {
		this.secondaryAction = secondaryAction;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
