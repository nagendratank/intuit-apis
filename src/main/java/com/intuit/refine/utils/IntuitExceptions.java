package com.intuit.refine.utils;

public class IntuitExceptions extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3695038307312400295L;

	public IntuitExceptions(Throwable cause) {
		super(cause);
	}

}
