package com.intuit.refine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.WorkdayReport;
import com.intuit.refine.service.ReportsService;

@RestController
@RequestMapping("/reports")
public class ReportController {
	@Autowired
	private ReportsService reportsService;

	@PostMapping("/workday")
	public String uploadWorkDayFile(@RequestParam("file") MultipartFile file) {
		reportsService.saveWorkDayReport(file);
		return "Uploaded SuccessFul";
	}

	@GetMapping("/workday")
	public List<WorkdayReport> getWorkDayFile() {
		return reportsService.getAllWorkDayReport();
	}

	@PostMapping("/alight")
	public String uploadAlightFile(@RequestParam("file") MultipartFile file) {
		reportsService.saveAlightReport(file);
		return "Uploaded SuccessFul";
	}

	@GetMapping("/alight")
	public List<AlightReport> getAlightFile() {
		return reportsService.getAllAlightReport();
	}
}
