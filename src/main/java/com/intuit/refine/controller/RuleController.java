package com.intuit.refine.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intuit.refine.jpa.entities.RuleEntity;
import com.intuit.refine.service.RuleService;

@RestController
@RequestMapping("/rules")
public class RuleController {

	@Autowired
	private RuleService ruleService;

	@PostMapping("/")
	public String saveRule(@RequestBody RuleEntity rule) {
		ruleService.saveRule(rule);
		return "Uploaded SuccessFul";
	}

	@PutMapping("/")
	public String updateRule(@RequestBody RuleEntity rule) {
		ruleService.saveRule(rule);
		return "Uploaded SuccessFul";
	}

	@GetMapping("/")
	public List<RuleEntity> getAllRules() {
		return ruleService.getAllRule();
	}

	@GetMapping("/{id}")
	public Optional<RuleEntity> getRule(@PathVariable(name = "id") int id) {
		return ruleService.getAllRule(id);
	}

	@GetMapping("/run")
	public String runRule() {
		return ruleService.runAllRule();
	}

	@DeleteMapping("/{id}")
	public void deleteRule(@PathVariable(name = "id") int id) {
		ruleService.deleteRule(id);
		ruleService.runAllRule();
	}
}
