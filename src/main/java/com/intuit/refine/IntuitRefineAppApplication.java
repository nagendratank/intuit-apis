package com.intuit.refine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntuitRefineAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntuitRefineAppApplication.class, args);
	}

}
