package com.intuit.refine.tests;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.ResourceUtils;

import com.intuit.refine.controller.ReportController;
import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.WorkdayReport;
import com.intuit.refine.service.ReportsService;

@RunWith(SpringRunner.class)
@WebMvcTest(ReportController.class)
public class ReportControllerTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private ReportsService service;

	@Test
	public void givenWorkdayEmployees_whenGetEmployees_thenReturnJsonArray() throws Exception {

		WorkdayReport workDayEmp = new WorkdayReport();
		workDayEmp.setEmployeeId(1);
		workDayEmp.setFirstName("Nagendra");
		workDayEmp.setLastName("Tank");
		workDayEmp.setEmploymentType("F");

		List<WorkdayReport> allEmployees = Arrays.asList(workDayEmp);
		Mockito.when(service.getAllWorkDayReport()).thenReturn(allEmployees);

		mvc.perform(get("/reports/workday").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].firstName", is(allEmployees.get(0).getFirstName())));
	}

	@Test
	public void givenAlightEmployees_whenGetEmployees_thenReturnJsonArray() throws Exception {

		AlightReport alightReport = new AlightReport();
		alightReport.setEmployeeId(1L);
		alightReport.setFirstName("Nagendra");
		alightReport.setLastName("Tank");

		List<AlightReport> allEmployees = Arrays.asList(alightReport);
		Mockito.when(service.getAllAlightReport()).thenReturn(allEmployees);

		mvc.perform(get("/reports/alight").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].firstName", is(allEmployees.get(0).getFirstName())));
	}

	@Test
	public void saveWorkDayReport() throws Exception {

		File file = ResourceUtils.getFile("src/test/resources/workday.csv");
		MockMultipartFile workdayfile = new MockMultipartFile("file", "test contract.pdf",
				MediaType.APPLICATION_PDF_VALUE, Files.readAllBytes(file.toPath()));

		mvc.perform(MockMvcRequestBuilders.multipart("/reports/workday").file("file", workdayfile.getBytes())
				.characterEncoding("UTF-8")).andExpect(status().isOk());

	}

	@Test
	public void saveAlightReport() throws Exception {

		File file = ResourceUtils.getFile("src/test/resources/alight.csv");
		MockMultipartFile workdayfile = new MockMultipartFile("file", "test contract.pdf",
				MediaType.APPLICATION_PDF_VALUE, Files.readAllBytes(file.toPath()));

		mvc.perform(MockMvcRequestBuilders.multipart("/reports/alight").file("file", workdayfile.getBytes())
				.characterEncoding("UTF-8")).andExpect(status().isOk());

	}
}
