package com.intuit.refine.tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.WorkdayReport;
import com.intuit.refine.jpa.repository.AlightReportDAO;
import com.intuit.refine.jpa.repository.WorkdayReporDAO;
import com.intuit.refine.service.ReportsService;
import com.intuit.refine.service.impl.ReportsServiceImpl;

@RunWith(SpringRunner.class)
public class ReportServiceTest {
	@TestConfiguration
	static class ReportServiceTestContext {

		@Bean
		public ReportsService reportsService() {
			return new ReportsServiceImpl();
		}
	}

	@Autowired
	private ReportsService reportService;

	@MockBean
	private WorkdayReporDAO workdayReporDAO;

	@MockBean
	private AlightReportDAO alightReportDAO;

	@Before
	public void setUp() {
		WorkdayReport workDayEmp = new WorkdayReport();
		workDayEmp.setEmployeeId(1);
		workDayEmp.setFirstName("Nagendra");
		workDayEmp.setLastName("Tank");
		workDayEmp.setEmploymentType("F");

		WorkdayReport workDayEmp2 = new WorkdayReport();
		workDayEmp2.setEmployeeId(2);
		workDayEmp2.setFirstName("Bala");
		workDayEmp2.setLastName("Krishna");
		workDayEmp2.setEmploymentType("P");

		List<WorkdayReport> employees = new ArrayList<WorkdayReport>();
		employees.add(workDayEmp);
		employees.add(workDayEmp2);
		Mockito.when(workdayReporDAO.findAll(Sort.by("employeeId"))).thenReturn(employees);

		AlightReport alightReport = new AlightReport();
		alightReport.setEmployeeId(1L);
		alightReport.setFirstName("Nagendra");
		alightReport.setLastName("Tank");

		AlightReport alightReport2 = new AlightReport();
		alightReport2.setEmployeeId(2L);
		alightReport2.setFirstName("Bala");
		alightReport2.setLastName("Krishna");

		List<AlightReport> alEmp = new ArrayList<AlightReport>();
		alEmp.add(alightReport);
		alEmp.add(alightReport2);
		Mockito.when(alightReportDAO.findAll(Sort.by("employeeId"))).thenReturn(alEmp);

	}

	@Test
	public void whenValidWorkdayId_thenEmployeeShouldBeFound() {
		List<WorkdayReport> found = reportService.getAllWorkDayReport();

		Assert.assertEquals("Assert Failed to match name", "Nagendra", found.get(0).getFirstName());
		Assert.assertEquals("Assert Failed to match employment type", "F", found.get(0).getEmploymentType());
	}

	@Test
	public void whenValidAlightId_thenEmployeeShouldBeFound() {
		List<AlightReport> found = reportService.getAllAlightReport();

		Assert.assertEquals("Assert Failed to match name", "Nagendra", found.get(0).getFirstName());
		Assert.assertEquals("Assert Failed to match employment type", "Tank", found.get(0).getLastName());
	}

	@Test
	public void whenAlightReportFile_thenGetAllEmployee() throws IOException {

		File file = ResourceUtils.getFile("src/test/resources/alight.csv");
		MockMultipartFile alightfile = new MockMultipartFile("file", "test contract.pdf",
				MediaType.APPLICATION_PDF_VALUE, Files.readAllBytes(file.toPath()));
		reportService.saveAlightReport(alightfile);
		List<AlightReport> found = reportService.getAllAlightReport();
		Assert.assertTrue(found.size() > 0);

	}

	@Test
	public void whenWorkDayReportFile_thenGetAllEmployee() throws IOException {

		File file = ResourceUtils.getFile("src/test/resources/workday.csv");
		MockMultipartFile workdayfile = new MockMultipartFile("file", "test contract.pdf",
				MediaType.APPLICATION_PDF_VALUE, Files.readAllBytes(file.toPath()));
		reportService.saveWorkDayReport(workdayfile);
		List<WorkdayReport> found = reportService.getAllWorkDayReport();
		Assert.assertTrue(found.size() > 0);

	}

	@Test(expected = RuntimeException.class)
	public void whenWorkDayReportFile_thenException() throws IOException {

		File file = ResourceUtils.getFile("src/test/resources/workday.csv");
		MockMultipartFile workdayfile = new MockMultipartFile("file", "test contract.pdf",
				MediaType.APPLICATION_PDF_VALUE, Files.readAllBytes(file.toPath()));
		reportService.saveAlightReport(workdayfile);
	}

	@Test(expected = RuntimeException.class)
	public void whenAlightReportFile_thenGetException() throws IOException {

		File file = ResourceUtils.getFile("src/test/resources/alight.csv");
		MockMultipartFile alightfile = new MockMultipartFile("file", "test contract.pdf",
				MediaType.APPLICATION_PDF_VALUE, Files.readAllBytes(file.toPath()));
		reportService.saveWorkDayReport(alightfile);
	}

}
