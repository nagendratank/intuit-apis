package com.intuit.refine.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.intuit.refine.jpa.entities.AlightReport;
import com.intuit.refine.jpa.entities.WorkdayReport;
import com.intuit.refine.jpa.repository.AlightReportDAO;
import com.intuit.refine.jpa.repository.RulesDAO;
import com.intuit.refine.jpa.repository.WorkdayReporDAO;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private WorkdayReporDAO reportDAO;

	@Autowired
	private AlightReportDAO alightReportDAO;

	@Autowired
	private RulesDAO ruleDAO;

	@Test
	public void whenFindById_thenReturnEmployeeWorkDay() {
		// given
		WorkdayReport workDayEmp = new WorkdayReport();
		workDayEmp.setEmployeeId(1);
		workDayEmp.setFirstName("Nagendra");
		workDayEmp.setLastName("Tank");
		workDayEmp.setEmploymentType("F");
		entityManager.persist(workDayEmp);
		entityManager.flush();

		WorkdayReport found = reportDAO.findById(workDayEmp.getEmployeeId()).orElse(null);

		Assert.assertTrue(found.getEmployeeId() == workDayEmp.getEmployeeId());
	}

	@Test
	public void whenFindAll_thenReturnEmployeeWorkDay() {
		// given
		WorkdayReport workDayEmp = new WorkdayReport();
		workDayEmp.setEmployeeId(1);
		workDayEmp.setFirstName("Nagendra");
		workDayEmp.setLastName("Tank");
		workDayEmp.setEmploymentType("F");
		entityManager.persist(workDayEmp);
		entityManager.flush();

		List<WorkdayReport> found = reportDAO.findAll();

		Assert.assertTrue(found.get(0).getEmployeeId() == workDayEmp.getEmployeeId());
	}

	@Test
	public void whenFindAllWithCondition_thenReturnEmployeeWorkDay() {
		// given
		WorkdayReport workDayEmp = new WorkdayReport();
		workDayEmp.setEmployeeId(1);
		workDayEmp.setFirstName("Nagendra");
		workDayEmp.setLastName("Tank");
		workDayEmp.setEmploymentType("F");
		entityManager.persist(workDayEmp);
		entityManager.flush();

		List<WorkdayReport> found = reportDAO.findEmployeeByCondition("employee_id is not null");

		Assert.assertTrue(found.get(0).getEmployeeId() == workDayEmp.getEmployeeId());
	}

	@Test
	public void whenFindById_thenReturnEmployeeAlight() {
		// given
		AlightReport alightReport = new AlightReport();
		alightReport.setEmployeeId(1L);
		alightReport.setFirstName("Nagendra");
		alightReport.setLastName("Tank");
		entityManager.persist(alightReport);
		entityManager.flush();

		AlightReport found = alightReportDAO.findById(alightReport.getEmployeeId()).get();

		Assert.assertTrue(found.getEmployeeId() == alightReport.getEmployeeId());
	}

	@Test
	public void whenFindAll_thenReturnEmployeeAlight() {
		// given
		AlightReport alightReport = new AlightReport();
		alightReport.setEmployeeId(1L);
		alightReport.setFirstName("Nagendra");
		alightReport.setLastName("Tank");
		entityManager.persist(alightReport);
		entityManager.flush();

		AlightReport found = alightReportDAO.findAll().get(0);

		Assert.assertTrue(found.getEmployeeId() == alightReport.getEmployeeId());
	}

}
