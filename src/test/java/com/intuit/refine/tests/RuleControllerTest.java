package com.intuit.refine.tests;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intuit.refine.controller.RuleController;
import com.intuit.refine.jpa.entities.RuleEntity;
import com.intuit.refine.service.EmailService;
import com.intuit.refine.service.RuleService;

@RunWith(SpringRunner.class)
@WebMvcTest(RuleController.class)
public class RuleControllerTest {
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private RuleService service;

	@MockBean
	private EmailService emailService;

	@Test
	public void getRuleEntitySaveRule() throws Exception {

		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");

		mvc.perform(post("/rules/").content(objectMapper.writeValueAsBytes(ruleEntity))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("Uploaded SuccessFul"));
	}

	@Test
	public void getRuleEntityUpdateRule() throws Exception {

		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");

		mvc.perform(put("/rules/").content(objectMapper.writeValueAsBytes(ruleEntity))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("Uploaded SuccessFul"));
	}

	@Test
	public void getAllRuleEntity() throws Exception {

		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");

		List<RuleEntity> allRules = Arrays.asList(ruleEntity);
		Mockito.when(service.getAllRule()).thenReturn(allRules);

		mvc.perform(get("/rules/").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].ruleName", is(allRules.get(0).getRuleName())));

	}

	@Test
	public void getRuleEntityByRuleId() throws Exception {

		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");

		List<RuleEntity> allRules = Arrays.asList(ruleEntity);
		Mockito.when(service.getAllRule(1)).thenReturn(Optional.of(ruleEntity));

		mvc.perform(get("/rules/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.ruleName", is(allRules.get(0).getRuleName())));

	}

	@Test
	public void deleteRuleEntityByRuleId() throws Exception {
		mvc.perform(delete("/rules/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void runRuleEntity() throws Exception {
		mvc.perform(get("/rules/run").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

}
