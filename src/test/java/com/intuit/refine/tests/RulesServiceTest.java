package com.intuit.refine.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import com.intuit.refine.jpa.entities.RuleEntity;
import com.intuit.refine.jpa.repository.AlightReportDAO;
import com.intuit.refine.jpa.repository.RulesDAO;
import com.intuit.refine.jpa.repository.WorkdayReporDAO;
import com.intuit.refine.service.EmailService;
import com.intuit.refine.service.RuleService;
import com.intuit.refine.service.impl.EmailServiceImpl;
import com.intuit.refine.service.impl.RuleServiceImpl;

@RunWith(SpringRunner.class)
public class RulesServiceTest {
	@Autowired
	private RuleService ruleService;

	@Autowired
	private EmailService emailService;

	@MockBean
	private WorkdayReporDAO workdayReporDAO;

	@MockBean
	private AlightReportDAO alightReportDAO;

	@MockBean
	private RulesDAO rulesDAO;

	@TestConfiguration
	static class RulesServiceTestContext {

		@Bean
		public RuleService reportsService() {
			return new RuleServiceImpl();
		}

		@Bean
		public EmailService emailServiceImpl() {
			return new EmailServiceImpl();
		}
	}

	@Before
	public void setUp() {
		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");
		ruleEntity.setEmail("nagendertank@gmail.com");

		List<RuleEntity> ruleEntities = new ArrayList<RuleEntity>();
		ruleEntities.add(ruleEntity);
		Mockito.when(rulesDAO.findAll(Sort.by("priority"))).thenReturn(ruleEntities);

		Mockito.when(rulesDAO.findById(ruleEntity.getRuleId())).thenReturn(Optional.of(ruleEntity));

	}

	@Test
	public void whenValidGetAllRule_thenRuleShouldBeFound() {
		List<RuleEntity> found = ruleService.getAllRule();
		Assert.assertEquals("Assert Failed to match rule name", "Test Rule", found.get(0).getRuleName());
		Assert.assertEquals("Assert Failed to match condition", "employee_id is not null", found.get(0).getCondition());
	}

	@Test
	public void whenValidRuleId_thenRuleShouldBeFound() {
		RuleEntity found = ruleService.getAllRule(1).get();
		Assert.assertEquals("Assert Failed to match rule name", "Test Rule", found.getRuleName());
		Assert.assertEquals("Assert Failed to match condition", "employee_id is not null", found.getCondition());
	}

	@Test
	public void whenRunRule_thenRuleShouldBeFound() {
		ruleService.runAllRule();
		RuleEntity found = ruleService.getAllRule(1).get();
		Assert.assertEquals("Assert Failed to match rule name", "Test Rule", found.getRuleName());
		Assert.assertEquals("Assert Failed to match condition", "employee_id is not null", found.getCondition());
	}

	@Test
	public void whenSaveRule_thenRuleShouldBeFound() {
		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");
		ruleService.saveRule(ruleEntity);
		RuleEntity found = ruleService.getAllRule(1).get();
		Assert.assertEquals("Assert Failed to match rule name", "Test Rule", found.getRuleName());
		Assert.assertEquals("Assert Failed to match condition", "employee_id is not null", found.getCondition());
	}

	@Test
	public void whenDeleteRule_thenRuleShouldBeFound() {
		RuleEntity ruleEntity = new RuleEntity();
		ruleEntity.setRuleId(1);
		ruleEntity.setRuleName("Test Rule");
		ruleEntity.setCondition("employee_id is not null");
		ruleEntity.setColor("#124134");
		ruleEntity.setPriority(2);
		ruleEntity.setTree("a==b");
		ruleService.deleteRule(ruleEntity.getRuleId());
		RuleEntity found = ruleService.getAllRule(1).get();
		Assert.assertEquals("Assert Failed to match rule name", "Test Rule", found.getRuleName());
		Assert.assertEquals("Assert Failed to match condition", "employee_id is not null", found.getCondition());
	}

}
